#!/bin/sh

set -e

export DEBCHANGE_RELEASE_HEURISTIC=changelog
export DEBCHANGE_FORCE_SAVE_ON_RELEASE=no

startdir="$(pwd)"

cleanup() {
	cd "$startdir"
	rm -f bug-template
	if dpkg -l | grep -q build-deps; then
		sudo apt-get autopurge -y '*build-deps'
	fi
}

trap cleanup INT HUP TERM EXIT

if [ -z "$1" ] || [ -z "$2" ] || ! [ "$1" -ge 1 ]; then
	cat >&2 <<EOF
Usage: $0 <batch_number> <file>

batch number is the number of the batch of 100 packages to upload, starting
with 1.

file is the file containing source packages to upload, one per line.

EOF
	exit 1
fi

components_count=$(apt-get indextargets 'ShortDesc: Sources' \
			--format '$(COMPONENT) $(SUITE)' | sort -u \
		| grep -Ec '(main|contrib|non-free) (unstable|experimental)')
if [ $components_count -lt 6 ]; then
	cat <<EOF
Error: you must have deb-src lines enabled in apt sources for all of
{unstable,experimental} {main,contrib,non-free} to proceed.
EOF
	exit 1
fi

# to avoid interactivity from mk-build-deps
echo 'APT::Get::Assume-Yes "true";' > build-deps-apt.conf
APT_CONFIG=$(pwd)/build-deps-apt.conf

sudo apt-get install reportbug devscripts dput

if [ ! -x /usr/sbin/sendmail ]; then
    echo "This script requires an MTA, set one up please"
    exit 1
fi

toolsdir=$(realpath $(dirname "$0"))

# because tail -n +N starts with line N rather than skipping N lines
start=$(($1 * 100 - 99))

sources=$(tail -n +$start "$2" | head -n 100 | awk '{ print $1 }')
for src in $sources; do
	src="${src%%:*}"

	if grep -q "^$src: \(package uploaded\|failed\)" \
		upload-nmus.log 2>/dev/null
	then
		continue
	fi

	if ! [ -e nmu_"$src".debdiff ]; then
		if ! [ -d "$src"-* ]; then
			apt-get source --only-source "$src"/unstable
			cd "$src"-*
			oldsrcver=$(dpkg-parsechangelog -S Version)
			if ! "$toolsdir"/time-t-me "$2"; then
				echo "Error: applying delta failed." >&2
				cd ..
				echo "$src: failed" >> upload-nmus.log
				rm -rf "$src"[_-]*
				continue
			fi
			dch -n 'Rename libraries for 64-bit time_t transition.'
			VISUAL=true dch -r -D experimental ''
		else
			cd "$src"-*
			# grab the version of the *previous* changelog entry
			oldsrcver=$(dpkg-parsechangelog -S Version -c2 -o1)
		fi
		sudo env APT_CONFIG=$APT_CONFIG mk-build-deps -i -r
		dpkg-buildpackage -S -uc -us
		srcver=$(dpkg-parsechangelog -S Version)
		# make sure the package from unstable builds with the patch
		# before we send it to the BTS
		DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS nocheck" \
			dpkg-buildpackage -uc -us
		# then immediately throw away the result
		rm -f ../*"${srcver#*:}"_*.*deb

		cd ..

		# size of changelog-only debdiff, means debian/control not
		# updated
		debdiff "$src"_${oldsrcver#*:}.dsc "$src"_${srcver#*:}.dsc \
			> nmu_"$src".debdiff || true
		rm -rf "$src"[_-]*
		# file too short, or missing mandatory content
		if [ $(cat nmu_"$src".debdiff | wc -l) -le 14 ] \
		   || ! grep -q '^+Provides:' nmu_"$src".debdiff
		then
			echo "Error: debian/control unchanged." >&2
			rm -f nmu_"$src".debdiff
			exit 1
		fi
	fi

	if ! grep -q "^$src: bug filed" upload-nmus.log 2>/dev/null
	then
		if querybts -b -s "$src" 2>/dev/null | grep -q 'time_t transition'
		then
			echo "$src: bug filed" >> upload-nmus.log
		fi
	fi

	if ! grep -q "^$src: bug filed" upload-nmus.log 2>/dev/null
	then
		# debdiff on disk so we skipped the above section that would
		# set the source version from the changelog
		if [ -z "$oldsrcver" ]; then
			oldsrcver=$(sed -n -e'/^---.*debian\/changelog/,/^---/ {
						/^ / { s/.*(//; s/).*//; p; q }
					 }' nmu_"$src".debdiff)
		fi

cat > bug-template <<EOF
NOTICE: these changes must not be uploaded to unstable yet!

Dear maintainer,

As part of the 64-bit time_t transition required to support 32-bit
architectures in 2038 and beyond
(https://wiki.debian.org/ReleaseGoals/64bit-time), we have identified
$src as a source package shipping runtime libraries whose ABI
either is affected by the change in size of time_t, or could not be
analyzed via abi-compliance-checker (and therefore to be on the safe
side we assume is affected).

To ensure that inconsistent combinations of libraries with their
reverse-dependencies are never installed together, it is necessary to
have a library transition, which is most easily done by renaming the
runtime library package.

Since turning on 64-bit time_t is being handled centrally through a change
to the default dpkg-buildflags (https://bugs.debian.org/1037136), it is
important that libraries affected by this ABI change all be uploaded close
together in time.  Therefore I have prepared a 0-day NMU for $src
which will initially be uploaded to experimental if possible, then to
unstable after packages have cleared binary NEW.

Please find the patch for this NMU attached.

If you have any concerns about this patch, please reach out ASAP.  Although
this package will be uploaded to experimental immediately, there will be a
period of several days before we begin uploads to unstable; so if information
becomes available that your package should not be included in the transition,
there is time for us to amend the planned uploads.

EOF

		reportbug --mode=expert \
			--no-query-bts --no-check-available \
			--no-debconf --no-check-installed \
			--no-bug-script --no-cc-menu \
			-T none \
			-P 'Tags: patch pending sid trixie' \
			-P 'User: debian-arm@lists.debian.org' \
			-P 'Usertags: time-t' \
			-S serious \
			-j "library ABI skew on upgrade" \
			-s "$src: NMU diff for 64-bit time_t transition" \
			-V "$oldsrcver" \
			-A nmu_"$src".debdiff \
			--body-file=bug-template \
			"src:$src"
		echo "$src: bug filed" >> upload-nmus.log
	fi

	# maybe there's a new version, or maybe we fall back to
	# unstable
	apt-get source --only-source "$src"/experimental \
	|| apt-get source --only-source "$src"/unstable

	cd "$src"-*

	# we filter out debian/changelog and regenerate it with
	# matching content, because if there is a new version then
	# that part of the diff will fail to apply; but if everything
	# else applies we should just upload to experimental anyway
	# with an appropriate bumped version.
	if ! filterdiff -x '*/debian/changelog' ../nmu_"$src".debdiff \
		| patch -p1 -f -F0
	then
		cd ..
		echo "$src: failed" >> upload-nmus.log
		rm -f nmu_"$src".debdiff
		rm -rf "$src"[_-]*
		continue
	fi

	dch -n 'Rename libraries for 64-bit time_t transition.'
	VISUAL=true dch -r -D experimental ''
	sed -i -e'1s/)/~exp1)/' debian/changelog
	srcver=$(dpkg-parsechangelog -S Version)

	# renaming of files + round-trip through diff+patch means lost
	# executable bits; restore them here.
	grep -l '^#!' debian/*t64*.install | xargs -r chmod a+x

	sudo env APT_CONFIG=$APT_CONFIG mk-build-deps -i -r
	dpkg-buildpackage -S
	sudo apt-get autopurge -y '*build-deps'

	cd ..

	dput ftp-master "$src"_*.changes

	echo "$src: package uploaded" >> upload-nmus.log

	rm -f nmu_"$src".debdiff
	rm -f *"${srcver#*:}"_*.*deb
	rm -rf "$src"[_-]*

done
