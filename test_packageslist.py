"""Test packageslist.py"""

import io
import tempfile
import unittest

from packagelist import Contents, SourceMapping


class TestContents(unittest.TestCase):
    # pylint: disable=missing-function-docstring
    """Test Contents object."""

    def test_from_file(self):
        contents_file = io.StringIO(
            "lib/arm-linux-gnueabihf/ld-linux-armhf.so.3             libs/libc6\n"
        )
        contents = Contents.from_file(contents_file)
        self.assertEqual(
            contents, {"lib/arm-linux-gnueabihf/ld-linux-armhf.so.3": {"libc6"}}
        )

    def test_from_file_with_dots(self):
        contents_file = io.StringIO(
            "usr/lib/python3/dist-packages/PyQt5/bindings/Qsci/Qsci.toml"
            " libs/pyqt5.qsci-dev\n"
        )
        contents = Contents.from_file(contents_file)
        expected = {
            "usr/lib/python3/dist-packages/PyQt5/bindings/Qsci/Qsci.toml": {
                "pyqt5.qsci-dev"
            }
        }
        self.assertEqual(contents, expected)

    def test_from_file_mutliple(self):
        contents_file = io.StringIO(
            "usr/libexec/sudo/audit_json.so                         "
            " admin/sudo,admin/sudo-ldap\n"
        )
        contents = Contents.from_file(contents_file)
        self.assertEqual(
            contents, {"usr/libexec/sudo/audit_json.so": {"sudo", "sudo-ldap"}}
        )

    def test_from_file_non_free(self):
        contents_file = io.StringIO(
            "usr/include/arb/AP_Tree.hxx                            "
            " non-free/libdevel/libarb-dev\n"
        )
        contents = Contents.from_file(contents_file)
        self.assertEqual(contents, {"usr/include/arb/AP_Tree.hxx": {"libarb-dev"}})

    def test_find_headers(self):
        contents_file = io.StringIO(
            "usr/include/collectd/core/config.h                     "
            " utils/collectd-dev\n"
            "usr/include/smclib/statemap.h                          "
            " libdevel/libsmc-dev\n"
            "usr/share/doc/libsbml5-examples/examples/c/util.h      "
            " doc/libsbml5-examples\n"
        )
        contents = Contents.from_file(contents_file)
        self.assertEqual(
            contents.find_packages_with_headers(), {"collectd-dev", "libsmc-dev"}
        )

    def test_find_libraries(self):
        contents_file = io.StringIO(
            "lib/systemd/system/greylistd.socket                    "
            " mail/greylistd\n"
            "usr/lib/libTogl.so                                     "
            " libdevel/libtogl-dev\n"
            "usr/lib/llvm-15/lib/libomp-15.so.5                     "
            " devel/libomp5-15\n"
        )
        contents = Contents.from_file(contents_file)
        self.assertEqual(
            contents.find_packages_with_libraries(), {"libtogl-dev", "libomp5-15"}
        )


class TestSourceMapping(unittest.TestCase):
    # pylint: disable=missing-function-docstring
    """Test SourceMapping class."""

    def test_from_packages_file(self):
        with tempfile.NamedTemporaryFile() as packages_file:
            packages_file.write(
                b"""\
Package: lib4ti2-dev
Source: 4ti2
Version: 1.6.10+ds-1
Installed-Size: 3948
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Architecture: armhf
Depends: lib4ti2-0 (= 1.6.10+ds-1)
Suggests: 4ti2-doc
Breaks: 4ti2 (<< 1.6.9+ds-6)
Description: mathematical tool suite for problems on linear spaces -- dev
Multi-Arch: same
Homepage: https://4ti2.github.io/
Description-md5: 9591f83eb40df70772f95b13b795e47c
Tag: devel::library, role::devel-lib
Section: libdevel
Priority: optional
Filename: pool/main/4/4ti2/lib4ti2-dev_1.6.10+ds-1_armhf.deb
Size: 553620
MD5sum: 56282c80b61a3ba39ca22fcd8e36c429
SHA256: 0d1e41212fcd3c1fb077867bee6a2567d7f928f6cdb0d64a24bb1d3f104fd51e
"""
            )
            packages_file.flush()
            source_mapping = SourceMapping.from_packages_file(packages_file.name)
        self.assertEqual(source_mapping, {"lib4ti2-dev": "4ti2"})

    def test_source_name_equls_binary_name(self):
        source_mapping = SourceMapping()
        with tempfile.NamedTemporaryFile() as packages_file:
            packages_file.write(
                b"""\
Package: distro-info
Version: 1.7
Installed-Size: 56
Maintainer: Benjamin Drung <bdrung@debian.org>
Architecture: armhf
Depends: distro-info-data (>= 0.59~), libc6 (>= 2.34)
Suggests: shunit2
Description: provides information about the distributions' releases
Description-md5: 5671943e370fd3dd7449d885e6363cee
Tag: devel::debian, implemented-in::c, interface::commandline, role::program,
 scope::utility
Section: devel
Priority: optional
Filename: pool/main/d/distro-info/distro-info_1.7_armhf.deb
Size: 17720
MD5sum: d967f1d946492e7ab082183a0d85a6bc
SHA256: 91b28a8c5b60ba2d85bcc0d85aa73423602aed91866e939c5344ae91509ed218
"""
            )
            packages_file.flush()
            source_mapping.add_packages_file(packages_file.name)
        self.assertEqual(source_mapping, {"distro-info": "distro-info"})

    def test_version_suffix(self):
        with tempfile.NamedTemporaryFile() as packages_file:
            packages_file.write(
                b"""\
Package: android-platform-frameworks-native-headers
Source: android-platform-tools (34.0.4-1)
Version: 1:34.0.4-1
Installed-Size: 724
Maintainer: Android Tools Maintainers <android-tools-devel@lists.alioth.debian.org>
Architecture: all
Description: Headers of android-platform-frameworks-native
Multi-Arch: foreign
Homepage: https://developer.android.com/studio/releases/platform-tools
Description-md5: 432d3ef0a04b352ab3c290d31ffd367b
Tag: devel::library, role::devel-lib
Section: libdevel
Priority: optional
Filename: pool/main/a/android-platform-tools\
/android-platform-frameworks-native-headers_34.0.4-1_all.deb
Size: 129756
MD5sum: 4d760d3d8f4f57471bb3e5a6e177eff2
SHA256: 22082296ffc5c1e9a890e746a7065bab43a9b79fcea66a69b407854bc48909a9
"""
            )
            packages_file.flush()
            source_mapping = SourceMapping.from_packages_file(packages_file.name)
        self.assertEqual(
            source_mapping,
            {"android-platform-frameworks-native-headers": "android-platform-tools"},
        )

    def test_map_binary_packages_to_sources(self):
        source_mapping = SourceMapping(
            {
                "lib4ti2-dev": "4ti2",
                "0ad-data-common": "0ad-data",
                "libafflib0v5": "afflib",
            }
        )
        self.assertEqual(
            source_mapping.map_binary_packages_to_sources(
                ["libafflib0v5", "lib4ti2-dev"]
            ),
            {"4ti2", "afflib"},
        )

    def test_filter_packages(self):
        source_mapping = SourceMapping(
            {
                "lib4ti2-dev": "4ti2",
                "0ad-data-common": "0ad-data",
                "libafflib0v5": "afflib",
            }
        )
        self.assertEqual(
            source_mapping.filter_packages(
                ["libafflib0v5", "lib4ti2-dev"], {"0ad-data", "afflib"}
            ),
            {"libafflib0v5"},
        )
